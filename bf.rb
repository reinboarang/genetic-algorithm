require "./chrom.rb"
require "./bflib.rb"

class BFGene < Gene

  @@gene_set = ['+','-','<','>','.',' ']

  def generate_new
    @value = @@gene_set.sample
  end

  def mutate_value
    case rand(100)
      when 0..74
        case @value
          when '+'
            @value = '-'
	  when '-'
	    @value = '+'
	  when '<'
	    @value = '>'
	  when '>'
	    @value = '<'
	  when '['
	    @value = ']'
	  when ']'
            @value = '['
	  else 
	    @value = @@gene_set.sample
	end
      else
        @value = @@gene_set.sample
    end
  end

  def to_s
    @value
  end

end

class BFChromosome < Chromosome
  
  def initialize(num_genes, mutation_rate)
    @genes = Array.new(num_genes) { BFGene.new }
    @mutation_rate = mutation_rate
  end

end

class BFPopulation < Population
  
  @@num_chroms = 100
  @@num_genes = 15
  @@mutation_rate = 10

  def initialize(num_chroms=@@num_chroms, num_genes=@@num_genes, mutation_rate=@@mutation_rate)
    @chroms = Array.new(num_chroms) { BFChromosome.new(num_genes, mutation_rate) }
  end

  def determine_fitness(chrom, target)
    score = 0
    bf_run = BF.new.compile(chrom.to_s)
    bf_run.run
    score = string_distance(target, bf_run.get_output[0..target.length-1])
    puts "Genes: #{chrom.to_s}"
    puts "Output: #{bf_run.get_output[0..target.length-1]}"
    puts "Fitness: #{score}"
    puts ""
    score
  end

  def mutate_population
    @chroms.each.with_index do |c,i| 
      c.mutate
    end
  end

  def process_generation(target="rat")
    average_fit = 0
    highest_fit = 0
    lowest_fit = 1000000
    fitnesses = @chroms.map.with_index do |c,i| 
      puts "Chromosome \##{i}"
      fit = determine_fitness(c, target)
      average_fit += fit
      highest_fit = fit if fit > highest_fit
      lowest_fit = fit if fit < lowest_fit
      [c, fit] 
    end
    puts "GENERATION STATS"
    puts "Average: #{average_fit / @chroms.length}"
    puts "Highest: #{highest_fit}"
    puts "Lowest: #{lowest_fit}"
    puts ""
    fitnesses.sort! { |a, b| b[1] <=> a[1] }
    fitnesses.slice!(0, (fitnesses.length/2).floor)
    @chroms = fitnesses.map { |f| f[0] } + Marshal.load(Marshal.dump(fitnesses.map { |f| f[0] } ))
    mutate_population
    return { high: highest_fit, low: lowest_fit, average: average_fit }
  end

end

def string_distance(a,b)
  distance = 0
  (a.length < b.length ? a.length : b.length).times do |i|
    distance += (a[i].ord - b[i].ord).abs
  end
  distance += (a.length - b.length).abs * 255
  distance
end
