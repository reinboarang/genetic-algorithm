#!/usr/bin/env ruby

class BF
  def initialize
    @output_set = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    @ops = create_ops
    @tape = Array.new(1024,0)
    @tp = 0
    @code = []
    @cp = 0
    @output = ""
    @ops_run = 0
  end

  def compile c
    c.split("").each do |o|
      if @ops.has_key? o
        @code << o
      end
    end
    return self
  end

  def run
    while @cp < @code.size and @ops_run < 1000
      run_op @code[@cp]
    end
    @cp = 0
    @ops_run = 0
  end
  
  def get_output
    @output
  end

  private

  def run_op op
    @ops[op].call
    @cp += 1
    @ops_run += 1
  end

  def get_input
    @tape[@tp] = STDIN.getc
    # getc returns nil on EOF. We want to use 0 instead.
    @tape[@tp] = 0 unless @tape[@tp]
  end

  def create_ops
    { ">" => Proc.new { @tp = (@tp == @tape.size - 1 ? 0 : @tp + 1) },
      "<" => Proc.new { @tp = (@tp == 0 ? @tape.size - 1 : @tp - 1) },
      "+" => Proc.new { @tape[@tp] == 255 ? @tape[@tp] = 0 : @tape[@tp] += 1 },
      "-" => Proc.new { @tape[@tp] == 0 ? @tape[@tp] = 255 : @tape[@tp] -= 1 },
      "." => Proc.new { @output << @output_set[(@tape[@tp] % @output_set.length)] if @tape[@tp] },
      "," => Proc.new { get_input },
      "[" => Proc.new { jump_to_close if @tape[@tp] == 0 },
      "]" => Proc.new { jump_to_open unless @tape[@tp] == 0 }
    }
  end

  def jump_to_close
    level = 1
    while @cp < @code.size
      @cp += 1
      if @code[@cp] == '['
        level += 1
      elsif @code[@cp] == ']'
        level -= 1
      end
      break if level == 0
    end
  end

  def jump_to_open
    level = 1
    while @cp >= 0
      @cp -= 1
      if @code[@cp] == ']'
        level += 1
      elsif @code[@cp] == '['
        level -= 1
      end
      break if level == 0
    end
  end
end

if __FILE__ == $0
  app =  BF.new
  File.open(ARGV[0], 'r') { |f|
    app.compile(f.read)
  }
  app.run
end
